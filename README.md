# Magento 2 Module: Insign/Helloworld

## Purpose
A simple Hello World Module for Magento 2 to demonstrate the Composer Workflow

## Installation

Add this repo to the composer.json file of your magento2 installation. For example like this:
```
	...
	"repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:nsimmler/magento2.module-helloworld.git"
        },
        ...
    ],
    ...
```

Then install the module (note: dev-master is the stability level which can be adjusted depending on your composer.json configuration)

```
composer require insign/module-helloworld:dev-master
./bin/magento module:enable
./bin/magento setup:upgrade
```

## Remarks

Make sure you have access to the repository. If it is a private repository make sure you have access (add ssh key or you have a user account with access to it).

Nothing has to be considered during or after installation.

## File Structure

This extension exposes the general file structure for a Magento 2 Extension.
```
   |-Block
   |-Controller
   |-Helper
   |-Model
   |-Observer
   |-Setup
   |-etc
   |---adminhtml
   |---frontend
   |-i18n
   |-view
   |---adminhtml
   |---frontend
```